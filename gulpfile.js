//package宣告
var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();

//個人的任務
gulp.task('sass', () => {
  return gulp.src('sass/*.scss')
  .pipe(sass())
  .pipe(gulp.dest('css'))
  .pipe(browserSync.stream());
});



//gulp.task('browser-sync', function(){});
//browser-sync 是開啟 server，就是初始化的
gulp.task('reload', () => {
  browserSync.reload();
});



//監測的項目
gulp.task('watch', () => {
  browserSync.init({
    server:'./'
  });
  gulp.watch("*.html").on('change', browserSync.reload);  
  gulp.watch('sass/*.scss', ['sass']);
  //gulp.watch('images/*.+(jpg|png)', ['sassImage']);
  


});


//將任務設置為使用命令執行
gulp.task('default', [ 'watch', 'sass', 'reload' ])