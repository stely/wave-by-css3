# wave-by-css3

這是一款使用CSS3 animation動畫和png圖片製作的css3線條波浪動畫效果。該特效中，使用了3張波浪線條的png圖片，以及少量的CSS代碼，製作出逼真的線條波浪動畫效果。

![圖片](https://bitbucket.org/stely/wave-by-css3/raw/bdede607b80ea75247b0faa1fa7d63e1336c95a1/00.jpg)

